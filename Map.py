from MapTile import MapTile
import Globals
import pygame
from random import randint
from AIUnit import AIUnit
import MathsHelpers
from __builtin__ import False
from PlayerUnit import PlayerUnit
from Chest import Chest

class Map(object):
##########################################
    def __init__(self, numRows, numColumns):
        self.tiles = []
        self.numRows = numRows
        self.numColumns = numColumns
        
        self.setupTiles()
        
        self.selectionCursor = pygame.image.load("Assets/selectionCursor.png")
        self.selectionCursorRect = self.selectionCursor.get_rect()
        self.attackCursor = pygame.image.load("Assets/attackCursor.png")
        self.attackCursorRect = self.attackCursor.get_rect()
        self.openCursor = pygame.image.load("Assets/openCursor.png")
        self.openCursorRect = self.openCursor.get_rect()
        
        self.moan = pygame.mixer.Sound("Assets/Sounds/ZombieMoan.wav")
        
        self.drawSelectCursor = False
        self.drawAttackCursor = False
        self.drawOpenCursor = False
        self.selectedUnit = None
        self.selectedUnitTileNum = None
        self.tileMoveOptions = []
        
        self.selectedAI = None
        self.AITarget = None
        self.AITargetTileNo = None
        self.selectedAITileNo = None
        
        self.objectUnderMouseTileNum = None
        self.objectUnderMouse = None
        self.objectUnderMouseIsFriendly = False
        
        #control
        self.lockedToUnit = False
        self.unitActionsAllowed = 2
        self.unitActionsRemaining = self.unitActionsAllowed
        self.allowControl = True #dont let players do things while actions are ongoing.
        self.doneForTheTurn = []
        
        self.killOff = None
        
        self.playerTurn = True
        
        self.friendlys = []
        self.enemies = []
        
        self.font = pygame.font.SysFont("monospace", 14)

##########################################        
    def setupTiles(self):
        currRow = 0
        currColumn = 0
        
        lastTopLeft = (1, 1)
         
        while(currRow < self.numRows):
            while(currColumn < self.numColumns):
                currentTile = MapTile(lastTopLeft)
                self.tiles.append(currentTile)
                lastTopLeft = (lastTopLeft[0] + currentTile.getSize()[0] + 1, lastTopLeft[1])
                currColumn += 1
            
            lastTopLeft = (1, lastTopLeft[1] + currentTile.getSize()[1] + 1)
            currColumn = 0
            currRow += 1

##########################################             
    def getTileByIndex(self, index):
        return self.tiles[index]

##########################################    
    def getTileByCoord(self, coord):
        for tile in self.tiles:
            if(tile.coordinateIsInside(coord)):
                return tile

##########################################     
    def getTilePositionInVectByCoord(self, coord):
        tilePos = 0 
        for tile in self.tiles:
            if(tile.coordinateIsInside(coord)):
                return tilePos
            tilePos += 1

##########################################             
    def addCoverItemToMap(self, item):
        tileToAddTo = self.getTileByCoord(item.getPosition())
        tileToAddTo.addCover(item)

##########################################         
    def addFriendlyUnitToMap(self, unit):
        tileToAddTo = self.getTileByCoord(unit.getPosition())
        tileToAddTo.addFriendlyUnit(unit)

##########################################          
    def addAIUnitToMap(self, unit):
        tileToAddTo = self.getTileByCoord(unit.getPosition())
        tileToAddTo.addAiUnit(unit)

##########################################         
    def addChestToMap(self, chest):
        tileToAddTo = self.getTileByCoord(chest.getPosition())
        tileToAddTo.addChest(chest)

##########################################          
    def moveFriendlyUnitBetweenTiles(self, unit, oldTile):
        mousePos = pygame.mouse.get_pos()
        
        newTilePos = self.getTilePositionInVectByCoord(mousePos)
        newTile = self.tiles[newTilePos]
        
        if(newTilePos in self.tileMoveOptions):
            oldTile.removeFriendlyUnit(unit)
            unit.setDestination(mousePos)
            newTile.addFriendlyUnit(unit)
            self.selectedUnitTileNum = newTilePos
            self.generalTakeActionTasks()

##########################################            
    def moveAiUnitBetweenTiles(self, unit, oldTile):
        newTilePos = self.getTilePositionInVectByCoord(unit.getDestination())
        newTile = self.tiles[newTilePos]
        
        oldTile.removeAiUnit(unit)
        newTile.addAiUnit(unit)
        self.selectedAITileNo = newTilePos
        
        self.generalTakeActionTasks()

##########################################               
    def handleCombat(self, attacker, defender, attackerTileNum, defenderTileNum):
        #get dice for defender
        if(self.checkRange(attacker.getMaxRange(), attackerTileNum, defenderTileNum)):
            defenderDice = 3
            if(self.tiles[defenderTileNum].tileProvidesCover):
                defenderDice += 1
            
            #get dice for attacker
            attackerDice = 4
            #think of some conditions such as upgrades.
            
            #get number of saves (range will go from 1 to defender dice -1 so we need to add one here).
            numSaves = defender.getArmourValue() #get some free saves based on armour.
            for i in range(1, (defenderDice + 1)):
                diceRoll = randint(1,8)
                if (diceRoll >= defender.getDefenseValue()):
                    numSaves += 1
            
            #get number of hits
            numHits = 0
            for i in range(1, (attackerDice + 1)):
                diceRoll = randint(1, 8)
                if(diceRoll >= attacker.getAttackValue()):
                    numHits += 1
            
            #compare with armour and do damage
            damageDone = numHits - numSaves
            
            if(damageDone > 0):
                defender.takeDamage(damageDone)
                if(defender.isDead):
                    #todo something to demonstrate death, fade out or new sprite or something.
                    self.killOff = (self.tiles[defenderTileNum], defender)
                    #self.tiles[self.objectUnderMouseTileNum].removeAiUnit(self.objectUnderMouse)
    
                
            attacker.handleCombat(defender.getRect())
            self.generalTakeActionTasks()

##########################################         
    def checkRange(self, weaponRange, checkerTileNo, targetTileNo):
        #weaponRange = self.selectedUnit.getMaxRange()
        distance = (weaponRange *self.numRows) + weaponRange
        minPosInGrid = checkerTileNo - distance
        maxPosInGrid = checkerTileNo + distance
        
        if(targetTileNo >= minPosInGrid and
           targetTileNo <= maxPosInGrid):
            return True
        
        return False  
            
##########################################         
    def checkMouseOver(self):
        mousePos = pygame.mouse.get_pos()
        
        i = 0
        for tile in self.tiles:
            item = tile.getItemUnderMouse(mousePos)
            friendlyUnit = None
            if(type(item) == AIUnit):
                friendlyUnit = False
            elif(type(item) == PlayerUnit):
                friendlyUnit = True
            
            if(not item == None):
                self.objectUnderMouse = item
                self.objectUnderMouseTileNum = i
                if(not friendlyUnit ==  None):
                    #its a unit friendly or enemy.
                    self.objectUnderMouse = item
                    self.objectUnderMouseIsFriendly = friendlyUnit
                    self.objectUnderMouseTileNum = i
                    if(friendlyUnit):
                        if(not item in self.doneForTheTurn):
                            pygame.mouse.set_visible(False)
                            self.drawSelectCursor = True
                    else:
                        if(not self.selectedUnit == None and self.checkRange(self.selectedUnit.getMaxRange(), self.selectedUnitTileNum, self.objectUnderMouseTileNum)):
                            pygame.mouse.set_visible(False)
                            self.drawAttackCursor = True
    
                    return
                else:
                    #its a chest
                    if(not self.selectedUnit == None and self.checkRange(0, self.selectedUnitTileNum, self.objectUnderMouseTileNum)):
                            pygame.mouse.set_visible(False)
                            self.drawOpenCursor = True
                    return

                
            i += 1
        
        pygame.mouse.set_visible(True)
        self.drawSelectCursor = False
        self.drawAttackCursor = False
        self.drawOpenCursor = False
        
        self.objectUnderMouse = None
        self.objectUnderMouseIsFriendly = False

#######################################################        
    def getNumFriendlys(self):
        friendlyCount = 0
        for tile in self.tiles:
            friendlyCount += tile.getNumFriendlys()
        
        return friendlyCount

#######################################################       
    def getAllFriendlys(self):
        friendlys = []
        for tile in self.tiles:
            friendlys += tile.getAllFriendlyUnits()
        return friendlys
 
 #######################################################       
    def getAllEnemies(self):
        enemies = []
        for tile in self.tiles:
            enemies += tile.getAllAiUnits()
        return enemies

#######################################################             
    def getNumAis(self):
        aiCount = 0
        for tile in self.tiles:
            aiCount += tile.getNumAis()
        
        return aiCount

#######################################################       
    def getTilesThatAreValidMovePositions(self):
        if(not self.selectedUnit == None):
            #tiles that can be moved to are (unless we are on a border we dont loop the map): 
            # -(width + 1) -width -(width-1)
            # -1  x  +1 
            # +(width - 1) +width +(width + 1)
            canGoLeft = False
            canGoRight = False
            canGoUp = False
            canGoDown = False
            
            if(not (self.selectedUnitTileNum % self.numRows == 0)):
                canGoLeft = True
            
            if(not(self.selectedUnitTileNum < self.numRows)):
                canGoUp = True
            
            if(not(self.selectedUnitTileNum >= (self.numColumns -1) * self.numRows )):
                canGoDown = True
                
            if(not(self.selectedUnitTileNum % self.numRows == (self.numRows - 1))):
                canGoRight = True
                
            if(canGoLeft):
                self.tileMoveOptions.append(self.selectedUnitTileNum -1)
                if(canGoUp):
                    self.tileMoveOptions.append(self.selectedUnitTileNum -(self.numColumns + 1))
                if(canGoDown):
                    self.tileMoveOptions.append(self.selectedUnitTileNum +(self.numColumns - 1))
                
            if(canGoRight):
                self.tileMoveOptions.append(self.selectedUnitTileNum + 1)
                if(canGoUp):
                    self.tileMoveOptions.append(self.selectedUnitTileNum -(self.numColumns - 1))
                if(canGoDown):
                    self.tileMoveOptions.append(self.selectedUnitTileNum +(self.numColumns + 1))
                    
            if(canGoUp):
                self.tileMoveOptions.append(self.selectedUnitTileNum - self.numColumns)
                
            if(canGoDown):
                self.tileMoveOptions.append(self.selectedUnitTileNum + self.numColumns)
               
 
#######################################################               
    def generalTakeActionTasks(self):
        if(not self.lockedToUnit):
            self.lockedToUnit = True
        
        self.unitActionsRemaining -= 1
        self.allowControl = False
        
#######################################################             
    def checkActionComplete(self, unit):
        if( (not self.selectedUnit == None) or (not self.selectedAI == None) ):
            if(unit.hasReachedDestination() and unit.combatHasFinished()):
                self.allowControl = True
                
                if(not self.killOff == None):
                    if(type(self.killOff[1]) == AIUnit):
                        self.killOff[0].removeAiUnit(self.killOff[1])
                        self.enemies.remove(self.killOff[1])
                    else:
                        self.killOff[0].removeFriendlyUnit(self.killOff[1])
                        self.AITarget = None
                        self.AITargetTileNo = None
                        self.friendlys.remove(self.killOff[1])
                        self.AITarget, ignore = self.getClosestFriendlyToEnemy(self.selectedAI)
                        if(not self.AITarget == None):
                            self.AITargetTileNo = self.getTilePositionInVectByCoord(self.AITarget.getCenterPosition())
                            self.selectedAITileNo = self.getTilePositionInVectByCoord(self.selectedAI.getCenterPosition())
                        
                        
                    self.killOff[0].addBloodStain(self.killOff[1].getCenterPosition())
                    self.killOff = None
                
            if((self.allowControl) and self.unitActionsRemaining == 0):
                self.handleUnitCompleteForTurn()

#######################################################                 
    def handleUnitCompleteForTurn(self):
        if(self.playerTurn):
            self.selectedUnit.deselect()
            self.doneForTheTurn.append(self.selectedUnit)
            self.selectedUnit = None
            self.lockedToUnit = False
        else:
            self.selectedAI.deselect()
            self.doneForTheTurn.append(self.selectedAI)
            self.selectedAI = None
            self.lockedToUnit = False
            self.AITarget = None
        
        self.unitActionsRemaining = self.unitActionsAllowed
        
        #toggle the player turn, but if your outta guys, the other guy gets to keep going. 
        if(self.playerTurn):
            for enemy in self.enemies:
                if(not enemy in self.doneForTheTurn):
                    self.playerTurn = False
        else:
            for friend in self.friendlys:
                if(not friend in self.doneForTheTurn):
                    self.playerTurn = True
        
        #if(not self.playerTurn):
        #    self.allowControl = False
        self.allowControl = True
            

#######################################################           
    def checkTurnComplete(self):
        for friendly in self.friendlys:
            if not friendly in self.doneForTheTurn:
                return False
                
        
        for enemy in self.enemies:
            if(not enemy in self.doneForTheTurn):
                return False
        
        return True

#######################################################        
    def startNewTurn(self):
        self.doneForTheTurn = []
        if(self.selectedUnit):
            self.selectedUnit.deselect()
            self.selectedUnit = None 
            self.selectedUnitTileNum = None
        if(self.selectedAI):
            self.selectedAI.deselect()
            self.selectedAI = None
            self.selectedAITileNo = None
        
        self.playerTurn = True
               
#######################################################
###############Generic Calls###########################
#######################################################   
    def draw(self):
        currTileNo = 0
        for tile in self.tiles:
            if(currTileNo in self.tileMoveOptions):
                tile.draw(True)
            else:
                tile.draw(False)
            currTileNo += 1
        
        #TODO: this is ghastly, time permitting do it better by not moving the guys onto their new tile until they reach it.     
        for tile in self.tiles:
            tile.drawObjectsOnTile()
            
        if(self.drawSelectCursor):
            self.selectionCursorRect.center = pygame.mouse.get_pos()
            Globals.screen.blit(self.selectionCursor, self.selectionCursorRect)
            
        if(self.drawAttackCursor):
            self.attackCursorRect.center = pygame.mouse.get_pos()
            Globals.screen.blit(self.attackCursor, self.attackCursorRect)
            
        if(self.drawOpenCursor):
            self.openCursorRect.center = pygame.mouse.get_pos()
            Globals.screen.blit(self.openCursor, self.openCursorRect)
            
        #draw turn instruction
        if(self.playerTurn):
            stringToDraw = "Your Turn"
        else:
            stringToDraw = "Enemy Turn"
            
        render = self.font.render(stringToDraw, 1, pygame.Color("Yellow"))
        renderRect = render.get_rect()
        renderRect.topright = (Globals.screenSize[0], 0)
        Globals.screen.blit(render, renderRect)
        
        nextTop = renderRect.bottom + 2
        #draw health of object under reticle
        if(not self.objectUnderMouse == None and not self.objectUnderMouseIsFriendly == None):
            colourToUse = pygame.Color("yellow")
            if(not self.objectUnderMouseIsFriendly):
                colourToUse = pygame.Color("red")
                
            render = self.font.render("Object under cursors current health: " + str(self.objectUnderMouse.currentHealth), 1, colourToUse)
            renderRect = render.get_rect()
            renderRect.topright = (Globals.screenSize[0], nextTop)
            Globals.screen.blit(render, renderRect)

##########################################              
    def update(self):
        self.friendlys = self.getAllFriendlys()
        self.enemies = self.getAllEnemies()
        
        self.tileMoveOptions = []
        self.checkMouseOver()
        self.getTilesThatAreValidMovePositions()
        
        if(self.playerTurn):
            self.checkActionComplete(self.selectedUnit)
        else:
            self.checkActionComplete(self.selectedAI)
            if(len(self.friendlys) == 0): #if we have no players left, just return and we will get defeat. 
                return
        
        if(self.checkTurnComplete()):
            self.startNewTurn()
            
        if(not self.playerTurn):  
            self.takeAiTurn()
        
        for tile in self.tiles:
            tile.update()

##########################################    
    def takeAiTurn(self):
        if(self.allowControl):
            if(self.selectedAI == None):
                self.chooseEnemyToActivate()
            else:
                if( self.checkRange(self.selectedAI.getMaxRange(), self.selectedAITileNo, self.AITargetTileNo) ):
                    self.handleCombat(self.selectedAI, self.AITarget, self.selectedAITileNo, self.AITargetTileNo)
                else:
                    #get the next tile along towards the target. 
                    tileSize = self.tiles[self.selectedAITileNo].getSize() #tiles will ALWAYS be square, so we are safe assuming width and height are the same
                    self.selectedAI.setDestinationViaTarget(self.AITarget, tileSize[0])
                    self.moveAiUnitBetweenTiles(self.selectedAI, self.tiles[self.selectedAITileNo])
            

##########################################         
    def handleEvent(self, event):
        if(self.allowControl and self.playerTurn):
            if(event.type == pygame.MOUSEBUTTONDOWN):
                if(event.button == 1):
                    if(not self.objectUnderMouse == None):
                        if(self.objectUnderMouseIsFriendly):
                            if(not self.lockedToUnit and not self.objectUnderMouse in self.doneForTheTurn):
                                self.objectUnderMouse.select()
                                if( (not self.selectedUnit == None) and (not self.selectedUnit == self.objectUnderMouse) ):
                                    self.selectedUnit.deselect()
                                    
                                self.selectedUnitTileNum = self.objectUnderMouseTileNum
                                self.selectedUnit = self.objectUnderMouse
                if(event.button == 3):
                    if(not self.selectedUnit == None):
                        if(not self.objectUnderMouse):
                            self.moveFriendlyUnitBetweenTiles(self.selectedUnit, self.tiles[self.selectedUnitTileNum]) 
                        else:
                            isChest = (type(self.objectUnderMouse) == Chest)
                            if(isChest and self.checkRange(0, self.selectedUnitTileNum, self.objectUnderMouseTileNum)):
                                self.handleChestOpen()
                            if(not self.objectUnderMouseIsFriendly and not isChest):
                                self.handleCombat(self.selectedUnit, self.objectUnderMouse, self.selectedUnitTileNum, self.objectUnderMouseTileNum)
            elif event.type == pygame.KEYDOWN:
                if(event.key == pygame.K_SPACE):
                    #skip the rest of his turn.
                    self.unitActionsRemaining = 0

##########################################
    def handleChestOpen(self):
        self.objectUnderMouse.open(self.selectedUnit)
        self.generalTakeActionTasks()
        if(self.selectedUnit.hasBeenGassed):
            self.selectedUnit.isDead
            self.tiles[self.selectedUnitTileNum].removeFriendlyUnit(self.selectedUnit)
            newZombie = AIUnit(AIUnit.SOLDIERZOMBIE, self.selectedUnit.getPosition())
            self.tiles[self.selectedUnitTileNum].addAiUnit(newZombie)
            self.doneForTheTurn.append(newZombie)
            self.friendlys.remove(self.selectedUnit)
            self.enemies.append(newZombie)
            self.handleUnitCompleteForTurn()
            self.moan.play()

            
                                
##########################################
    def chooseEnemyToActivate(self):
        #we always activate the enemy that is avaliable and closest to a friendly unit.  
        options = self.enemies
        
        currentClosestDistance = self.numColumns * 100 #arbitrarily large number
        currentClosestFriendly = None
        currentClosestActivationTarget = None
        for option in options:
            if(option in self.doneForTheTurn):
                continue
            
            friendly, distance = self.getClosestFriendlyToEnemy(option)
            if(distance < currentClosestDistance):
                currentClosestFriendly = friendly
                currentClosestDistance = distance
                currentClosestActivationTarget = option
        
        self.selectedAI = currentClosestActivationTarget
        if(not self.selectedAI == None):
            self.selectedAI.select()
            self.AITarget = currentClosestFriendly
        
            self.AITargetTileNo = self.getTilePositionInVectByCoord(self.AITarget.getCenterPosition())
            self.selectedAITileNo = self.getTilePositionInVectByCoord(self.selectedAI.getCenterPosition())

##########################################        
    def getClosestFriendlyToEnemy(self, enemy):
        currentClosestDistance = self.numColumns * 100 #arbitrarily large number
        currentClosestFriendly = None
        
        for friendly in self.friendlys:
                distance = MathsHelpers.distanceBetweenCoordinates(enemy.getCenterPosition(), friendly.getCenterPosition())
                if(distance < currentClosestDistance):
                    currentClosestFriendly = friendly
                    currentClosestDistance = distance
        
        return currentClosestFriendly, currentClosestDistance
        
                    
                        
                    
                    
        
        
