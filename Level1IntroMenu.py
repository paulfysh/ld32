import sys
from Menu import Menu
from Level1 import Level1

class Level1IntroMenu(object):
########################################################################
    def __init__(self):
        self.Menu = Menu()
        self.Menu.addMenuOption("Understood", self.nextLevel)
        self.Menu.addMultiLineAdditionalText("Commander, we have received word of bio organic weapons attacking a civilian population" )
        self.Menu.addMultiLineAdditionalText("in the desert, we are investigating the source of the outbreak but we need you to " )
        self.Menu.addMultiLineAdditionalText("go and destroy them all. Use of lethal force is authorised")

        self.Menu.setTitle("Level 1")
        self.nextScreen = None
        

########################################################################
    def nextLevel(self):
        self.nextScreen = Level1()

        
#######################################################
###############Generic Calls###########################
#######################################################         
    def draw(self):
        self.Menu.draw()

#######################################################          
    def update(self):
        self.Menu.update()

#######################################################          
    def handleEvent(self, event):
        self.Menu.handleEvent(event)