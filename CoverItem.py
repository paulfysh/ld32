import pygame
import Globals

from GameObject import GameObject

class CoverItem(GameObject):
    
    UPRIGHTBARREL = 1
    SIDEWAYSBARREL = 2
    SANDBAGS = 3
    
##########################################
    def __init__(self, typeOfCover, topLeft):
        super(CoverItem, self).__init__(topLeft)
        if(typeOfCover == CoverItem.UPRIGHTBARREL):
            self.image = pygame.image.load("Assets/UprightBarrelScaled.png")
        elif(typeOfCover == CoverItem.SIDEWAYSBARREL):
            self.image = pygame.image.load("Assets/SidewaysBarrelScaled.png")
        elif(typeOfCover == CoverItem.SANDBAGS):
            self.image = pygame.image.load("Assets/sandbagWallScaled.png")
        
        self.positionRect = self.image.get_rect()
        self.positionRect.topleft = topLeft
        
##########################################        
    def draw(self):
        Globals.screen.blit(self.image, self.positionRect)
        