import Globals
import pygame
import MathsHelpers

class GameObject(object):
    def __init__(self, topLeft):
        self.topLeft = topLeft
        
        #defaults mean you cant ever hit anything or save anything. override by children.  
        self.attackValue = 9
        self.defenseValue = 9
        self.armour = 0
        self.maxRange = 0 #in tiles
        
        #health
        self.maxHealth = 2
        self.currentHealth = self.maxHealth
        self.isDead = False
        
        #selection
        self.selectedBox = pygame.image.load("Assets/selectedBox.png")
        self.selected = False
        
        self.realCurrentPosition = (float(0), float(0)) #need to store floats of where we really are in case of rounding on fragmented movement updates
        
        #movement
        self.currentDestination = None #override in the children.

########################################## 
    def getPosition(self):
        #return self.topLeft
        return self.positionRect.topleft

##########################################  
    def getCenterPosition(self):
        return self.positionRect.center
 
##########################################     
    def setPositionCenter(self, newPos):
        self.positionRect.center = newPos

##########################################         
    def getRect(self):
        return self.positionRect
    

##########################################  
    def draw(self):        
        if(self.selected):
            scaledSelectedBox = pygame.transform.scale(self.selectedBox, self.positionRect.size)
            scaledSelectedBoxRect = scaledSelectedBox.get_rect()
            scaledSelectedBoxRect.center = self.positionRect.center
            Globals.screen.blit(scaledSelectedBox, scaledSelectedBoxRect)
        

##########################################    
    def getSize(self):
        return self.positionRect.size    
    
##########################################    
    def coordinateIsInside(self, coord):
        if( (coord[0] >= self.positionRect.left and coord[0] <= self.positionRect.right) and
            (coord[1] >= self.positionRect.top and coord[1] <= self.positionRect.bottom) ):
            return True
        return False

##########################################              
    def update(self):
        if(not self.positionRect.center == self.currentDestination):
            moveThisTick = self.movePerSecond / Globals.FPS
            if(MathsHelpers.distanceBetweenCoordinates(self.currentDestination, self.positionRect.center ) > moveThisTick):
                valuesToAdd = MathsHelpers.getXYForAngleAndRange(self.currentAngle, moveThisTick)
                self.realCurrentPosition = (self.realCurrentPosition[0] + valuesToAdd[0], self.realCurrentPosition[1] + valuesToAdd[1])
                self.positionRect.center = self.realCurrentPosition
                self.updateAngle(self.currentDestination)
            else:
                self.positionRect.center = self.currentDestination
            
            if(Globals.currentTime >= self.lastStepChange + self.intervalBetweenSteps):
                self.workingImage = self.walkingList[0]
                self.walkingList.remove(self.workingImage)
                self.walkingList.append(self.workingImage)
                self.lastStepChange = Globals.currentTime
    
    
########################################## 
    def getAttackValue(self):
        return self.attackValue

##########################################     
    def getDefenseValue(self):
        return self.defenseValue

##########################################     
    def getArmourValue(self):
        return self.armour   

##########################################      
    def getMaxRange(self):
        return self.maxRange

##########################################
    def takeDamage(self, amount):
        self.currentHealth -= amount
        
        if(self.currentHealth <= 0):
            self.isDead = True
            
##########################################       
    def select(self):
        self.selected = True

##########################################         
    def deselect(self):
        self.selected = False
        
##########################################        
    def hasReachedDestination(self):
        return (self.currentDestination == self.positionRect.center)
    
##########################################          
    def setDestination(self, pos):
        self.currentDestination = pos
        self.updateAngle(self.currentDestination)
        self.lastStepChange = Globals.currentTime - self.intervalBetweenSteps
        #self.currentAngle = MathsHelpers.getAngleToFaceCoord(pos, self.positionRect.center[0], self.positionRect.center[1])

##########################################        
    def getDestination(self):
        return self.currentDestination
        
##########################################                 
    def updateAngle(self, coord):
        tempAngle = MathsHelpers.getAngleToFaceCoord(coord, self.positionRect.center[0], self.positionRect.center[1])
        if(not tempAngle == None):
            self.currentAngle = MathsHelpers.handleAngleBoundary(tempAngle)
        
            if(not self.positionRect.collidepoint(coord)):
                self.currentImage = pygame.transform.rotate(self.workingImage, (self.currentAngle * -1))
                newRect = self.currentImage.get_rect()
                newRect.center = self.positionRect.center
                self.positionRect = newRect


        