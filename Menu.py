import pygame
import Globals

class Menu(object):
########################################################################
    def __init__(self):
        self.font = pygame.font.SysFont("monospace", 15)
        self.titleFont = pygame.font.SysFont("monospace", 30)
        
        self.lastAddedBottom = (Globals.screenSize[1] / 2)
        self.menuOptions = []
        self.currentSelection = 0
        self.newKeyPress = True
        
        self.additionalTextPos = None
        self.additionalText = None
        self.additionalTextSplit = []
        
        self.title = None
        self.titleTextPos = None
        
        #should never have to change this, because it is handled by the method callbacks.
        self.nextScreen = None
    
#######################################################        
    def addMenuOption(self, text, methodCallback):
        stringWidth, stringHeight = self.font.size(text)
        #topLeft = ( (Globals.screenSize[0] / 2) - (stringWidth / 2), self.lastAddedBottom)
        #bottomRight = ( (Globals.screenSize[0] / 2 + (stringWidth / 2), self.lastAddedBottom + stringHeight ))
        center = ( (Globals.screenSize[0] / 2), self.lastAddedBottom + (stringHeight / 2) )
        self.menuOptions.append( (text, methodCallback, center) ) #topLeft, bottomRight) )
        self.lastAddedBottom  = self.lastAddedBottom + stringHeight + 2

#######################################################             
    def setAdditionalText(self, text):
        stringWidth = self.font.size(text)[0]
        self.additionalTextPos = ( (Globals.screenSize[0] / 2) - (stringWidth / 2), Globals.screenSize[1] / 3)
        self.additionalText = text

#######################################################
    def addMultiLineAdditionalText(self, text):
        self.additionalTextSplit.append(text)
        
#######################################################        
    def setTitle(self, titleText):
        stringWidth = self.titleFont.size(titleText)[0]
        
        self.title = titleText
        self.titleTextPos = (Globals.screenSize[0] / 2 - stringWidth / 2, 1)


#######################################################
###############Generic Calls###########################
#######################################################         
    def draw(self):
        if(not self.additionalText == None):
            renderLabel = self.font.render(self.additionalText, 1, pygame.Color("Yellow"))
            renderLabelRect = renderLabel.get_rect()
            renderLabelRect.topleft = self.additionalTextPos
            Globals.screen.blit(renderLabel, renderLabelRect)
        elif(len(self.additionalTextSplit) > 0):
            lastPos = ( (Globals.screenSize[0] / 2), (Globals.screenSize[1] / 4) )
            for text in self.additionalTextSplit:
                renderLabel = self.font.render(text, 1, pygame.Color("Yellow"))
                renderLabelRect = renderLabel.get_rect()
                stringWidth, stringHeight = self.font.size(text)
                lastPos = ( ((Globals.screenSize[0] / 2) - (stringWidth / 2)), lastPos[1] + stringHeight) 
                renderLabelRect.topleft = lastPos
                Globals.screen.blit(renderLabel, renderLabelRect)
                
                
            
        if (not self.title == None):
            renderLabel = self.titleFont.render(self.title, 1, pygame.Color("Yellow"))
            renderLabelRect = renderLabel.get_rect()
            renderLabelRect.topleft = self.titleTextPos
            Globals.screen.blit(renderLabel, renderLabelRect)
        
        currentOptionNo = 0
        for option in self.menuOptions:
            colourToUse = pygame.Color("Yellow")
            if(currentOptionNo == self.currentSelection):
                colourToUse = pygame.Color("Red")
            
            renderLabel = self.font.render(option[0], 1, colourToUse)
            rect = renderLabel.get_rect()
            rect.center = option[2]
            Globals.screen.blit(renderLabel, rect)
            
            currentOptionNo += 1

#######################################################          
    def update(self):
        pass

#######################################################          
    def handleEvent(self, event):
        if(event.type == pygame.KEYDOWN):
            self.handleKeydown(event)
        elif (event.type == pygame.KEYUP):
            self.handleKeyup(event)
            

#######################################################
###############Control methods#########################
#######################################################
    def handleKeydown(self, event):
        if(self.newKeyPress):
            if(event.key == pygame.K_DOWN):            
                if(self.currentSelection + 1 > (len(self.menuOptions) - 1) ):
                    self.currentSelection = 0
                else:
                    self.currentSelection += 1
            elif(event.key == pygame.K_UP):
                if(self.currentSelection - 1 >= 0):
                    self.currentSelection -= 1
                else:
                    self.currentSelection = (len(self.menuOptions) - 1)
            elif(event.key == pygame.K_RETURN):
                self.menuOptions[self.currentSelection][1]()
        
        self.newKeyPress = False
        
        

#######################################################    
    def handleKeyup(self, event):
        self.newKeyPress = True

        