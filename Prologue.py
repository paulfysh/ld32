import pygame
import Globals
from Level1IntroMenu import Level1IntroMenu

class Prologue(object):
 
 #######################################################   
    def __init__(self):
        #images
        self.mapTileImage = pygame.image.load("Assets/SandTile.png")
        self.mapTileRect = self.mapTileImage.get_rect()
        
        self.gasCloudImage = pygame.image.load("Assets/GasCloudCircleScaled.png")
        self.scaledGasCloudImage = None
        self.gasCloudRect = self.gasCloudImage.get_rect()
        
        self.civImage = pygame.image.load("Assets/CivilianStationaryScaled.png")
        self.firstCivRect = self.civImage.get_rect()
        self.secondCivRect = self.civImage.get_rect()
        self.thirdCivRect = self.civImage.get_rect()
        
        self.zombieImage = pygame.image.load("Assets/ZombieStationaryScaled.png")
        self.firstZombieRect = self.zombieImage.get_rect()
        self.secondZombieRect = self.zombieImage.get_rect()
        self.thirdZombieRect = self.zombieImage.get_rect()
        
        self.closedChestImage = pygame.image.load("Assets/ClosedChestScaled.png")
        self.closedChestRect = self.closedChestImage.get_rect()
        
        self.openChestImage = pygame.image.load("Assets/OpenChestScaled.png")
        self.openChestRect = self.openChestImage.get_rect()
        
        self.fullScreenSurface = pygame.Surface(Globals.screenSize).convert()
        self.fullScreenSurface.fill(pygame.Color("Black"))
        self.fullScreenSurface.set_alpha(1)
        self.fullScreenSurfaceRect = self.fullScreenSurface.get_rect()
        self.fullScreenSurfaceRect.topleft = (0,0)
        
        #sounds (probably redo these before submitting :p)
        self.waterPumpSound = pygame.mixer.Sound("Assets/Sounds/waterPump.wav")
        self.waterPumpSound.play()
        
        self.savedSound = pygame.mixer.Sound("Assets/Sounds/saved.wav")
        self.coughSound = pygame.mixer.Sound("Assets/Sounds/cough.wav")
        self.moan = pygame.mixer.Sound("Assets/Sounds/ZombieMoan.wav")
        
        
        
        #flags
        self.savedSoundPlayed = False
        self.chestOpened = False
        self.civsStillAlive = True
        self.currentGasAlpha = 250
        self.coughed = False
        
        self.nextScreen = False
        
        #timers
        self.lastGasUpdate = None
        self.intervalInGasUpdates = 0.1
        self.coughEnds = None
        
        #setup methods
        self.setupEvents()
        self.handleObjectPositions()
        
 
 #######################################################       
    def handleObjectPositions(self):
        self.closedChestRect.center = (150,200 - self.closedChestRect.height / 2)
        self.openChestRect.center = (150,200 - self.openChestRect.height / 2)
        
        self.civImage = pygame.transform.rotate(self.civImage, 180)
        
        self.firstCivRect.center = (self.closedChestRect.center[0], self.closedChestRect.top - self.secondCivRect.height / 2)
        self.secondCivRect.center = (150 - self.firstCivRect.width / 2, self.closedChestRect.top - self.firstCivRect.height * 1.5)
        self.thirdCivRect.center = (150 + self.firstCivRect.width / 2, self.closedChestRect.top - self.firstCivRect.height * 1.5)
        
        self.zombieImage = pygame.transform.rotate(self.zombieImage, 180)
        
        self.firstZombieRect.center = self.firstCivRect.center
        self.secondZombieRect.center = self.secondCivRect.center
        self.thirdZombieRect.center = self.thirdCivRect.center
        
        self.scaledGasCloudImage = pygame.transform.scale(self.gasCloudImage, (10,10))
        self.gasCloudRect = self.scaledGasCloudImage.get_rect()
        self.gasCloudRect.center = self.openChestRect.center
        self.scaledGasCloudImage.set_alpha(250)

 #######################################################          
    def setupEvents(self):
        self.savedSoundPlayTime = Globals.currentTime + self.waterPumpSound.get_length() + 1
        self.chestOpenTime = self.savedSoundPlayTime + self.savedSound.get_length()
    
#######################################################
###############Generic Calls###########################
#######################################################   
    def update(self):
        if(self.savedSoundPlayTime <= Globals.currentTime and not self.savedSoundPlayed):
            self.savedSound.play()
            self.savedSoundPlayed = True
        
        if(self.chestOpenTime <= Globals.currentTime and not self.chestOpened):
            self.chestOpened = True
            self.lastGasUpdate = Globals.currentTime
            
        if(self.gasCloudRect.width < 150 and self.chestOpened):
            if(Globals.currentTime >= self.lastGasUpdate + self.intervalInGasUpdates):
                self.scaledGasCloudImage = pygame.transform.scale(self.gasCloudImage, (self.gasCloudRect.width + 5, self.gasCloudRect.height + 5))
                self.gasCloudRect = self.scaledGasCloudImage.get_rect()
                self.gasCloudRect.center = self.openChestRect.center
                self.lastGasUpdate = Globals.currentTime
        
        if(self.chestOpened and self.gasCloudRect.width >= 150):
            if(not self.coughed):
                self.coughSound.play()
                self.coughed = True
                self.coughEnds = Globals.currentTime + self.coughSound.get_length()
            
            self.scaledGasCloudImage.set_alpha(self.currentGasAlpha)
            
            if(self.currentGasAlpha > 0):
                self.currentGasAlpha -= 1
                
        if( (not self.coughEnds == None) and (self.coughEnds <= Globals.currentTime) and (self.civsStillAlive) ):
            self.civsStillAlive = False
            self.moan.play()
            
                
        if(self.fullScreenSurface.get_alpha() >= 250):
                self.completePrologue()
            

#######################################################        
    def draw(self):
        self.drawMap()
        self.drawChest()
        self.drawCivilians()
        self.drawGasCloud()
        self.drawFadingBlack()

#######################################################        
    def handleEvent(self, event):
        #we dont need to do anything here, its just an intro (possibly allow skip on enter)
        if(event.type == pygame.KEYDOWN):
            if(event.key == pygame.K_RETURN):
                self.completePrologue()

#######################################################                
    def completePrologue(self):
        self.moan.stop()
        self.waterPumpSound.stop()
        self.savedSound.stop()
        self.coughSound.stop()
        
        self.nextScreen = Level1IntroMenu() #we should use this to show level 1.    
                

#######################################################
###############rendering Calls#########################
#######################################################
    def drawMap(self):
        #draw an 8 by 8 Grid
        numRows = 3
        numColumns = 3
         
        currRow = 0
        currColumn = 0
        
        lastTopLeft = (1, 1)
         
        while(currRow < numRows):
            while(currColumn < numColumns):
                self.mapTileRect.topleft = lastTopLeft
                Globals.screen.blit(self.mapTileImage, self.mapTileRect)
                lastTopLeft = (lastTopLeft[0] + self.mapTileRect.width + 1, lastTopLeft[1])
                currColumn += 1
            
            lastTopLeft = (1, lastTopLeft[1] + self.mapTileRect.height + 1)
            currColumn = 0
            currRow += 1

#######################################################            
    def drawChest(self):
        if(self.chestOpened):
            Globals.screen.blit(self.openChestImage, self.openChestRect)
        else:
            Globals.screen.blit(self.closedChestImage, self.closedChestRect)

#######################################################            
    def drawCivilians(self):
        if(self.civsStillAlive):
            Globals.screen.blit(self.civImage, self.firstCivRect)
            Globals.screen.blit(self.civImage, self.secondCivRect)
            Globals.screen.blit(self.civImage, self.thirdCivRect)
        else:
            Globals.screen.blit(self.zombieImage, self.firstZombieRect)
            Globals.screen.blit(self.zombieImage, self.secondZombieRect)
            Globals.screen.blit(self.zombieImage, self.thirdZombieRect)

#######################################################             
    def drawGasCloud(self):
        if(self.chestOpened and self.civsStillAlive ):
            Globals.screen.blit(self.scaledGasCloudImage, self.gasCloudRect)

#######################################################             
    def drawFadingBlack(self):
        if(not self.civsStillAlive):
            self.fullScreenSurface.set_alpha(self.fullScreenSurface.get_alpha() + 1)
            Globals.screen.blit(self.fullScreenSurface, self.fullScreenSurfaceRect)
            
        