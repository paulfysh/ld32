import sys
from Menu import Menu

class InstructionMenu(object):
########################################################################
    def __init__(self):
        self.Menu = Menu()
        self.Menu.addMenuOption("Understood", self.backToMain)
        self.Menu.addMultiLineAdditionalText("Instructions: You are a team of soldiers fighting an army of bioweapons." )
        self.Menu.addMultiLineAdditionalText("Bioterrorism is tile based turn based strategy.")
        self.Menu.addMultiLineAdditionalText("You activate one soldier, and may perform two actions, ")
        self.Menu.addMultiLineAdditionalText("then the enemy can activate a soldier and perform 2 actions until all soldiers are done.")
        self.Menu.addMultiLineAdditionalText("Possible actions are: Move, Attack, Open (a chest). You may repeat actions or mix and match.")
        self.Menu.addMultiLineAdditionalText("being in a tile with cover makes you harder to hit (barells, sideways barells or sandbags.")
        self.Menu.addMultiLineAdditionalText("Chests contain loot. You can skip the prologue by hitting enter.")
        self.Menu.addMultiLineAdditionalText("You can quit at any time (without prompt!) by hitting escape.")
        self.Menu.addMultiLineAdditionalText("Hitting space ends your currently selected soldiers turn")
        self.Menu.setTitle("Instructions!")
        self.nextScreen = None
        
        #we get a circular import so it wont let me do it. This is a bit of a hack to get round it.
        #restartLevel = level1

########################################################################
    def backToMain(self):
        self.nextScreen = "Back"

        
#######################################################
###############Generic Calls###########################
#######################################################         
    def draw(self):
        self.Menu.draw()

#######################################################          
    def update(self):
        self.Menu.update()

#######################################################          
    def handleEvent(self, event):
        self.Menu.handleEvent(event)