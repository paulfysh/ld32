
import sys
#from Level1 import Level1
from Menu import Menu

class VictoryMenu(object):
########################################################################
    def __init__(self):
        self.displayString = "You won, the world is safe from my atrocious voice... for now."
        
        self.Menu = Menu()
        self.Menu.addMenuOption("Restart? (Dont worry I wont make you listen to my awful voices again)", self.restartGame)
        self.Menu.addMenuOption("quit happy", self.quitGame)
        self.Menu.setAdditionalText(self.displayString)
        self.Menu.setTitle("VICTORY!")
        self.nextScreen = None


########################################################################
    def restartGame(self):
        #we get a circular import so it wont let me do it. This is a bit of a hack to get round it.
        from Level1 import Level1
        self.nextScreen = Level1()

########################################################################
    def quitGame(self):
        sys.exit()
        
#######################################################
###############Generic Calls###########################
#######################################################         
    def draw(self):
        self.Menu.draw()

#######################################################          
    def update(self):
        self.Menu.update()

#######################################################          
    def handleEvent(self, event):
        self.Menu.handleEvent(event)