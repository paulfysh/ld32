import pygame, sys, time
import Globals
from Engine import Engine

pygame.init()

size = width, height = 1024, 768

screen = pygame.display.set_mode(size)
clock = pygame.time.Clock()

Globals.screenSize = size
Globals.screen = pygame.display.get_surface()
Globals.mixer = pygame.mixer.init()
Globals.currentTime = time.time()

gameEngine = Engine()

font = pygame.font.SysFont("monospace", 12)

while 1:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            if(event.key == pygame.K_ESCAPE):
                sys.exit()
        gameEngine.handleEvent(event)
    
    Globals.currentTime = time.time()
    Globals.FPS = clock.get_fps()
    screen.fill(pygame.Color("Black"))
    gameEngine.update()
    gameEngine.draw()
    
    renderLabel = font.render("FPS: " + str(Globals.FPS), 1, pygame.Color("Blue"))
    labelRect = renderLabel.get_rect()
    labelRect.topleft = (0,0)
    screen.blit(renderLabel, labelRect)
    
    pygame.display.flip()
    clock.tick(60)