import sys
from Menu import Menu
from Level2 import Level2

class Level2IntroMenu(object):
########################################################################
    def __init__(self):
        self.Menu = Menu()
        self.Menu.addMenuOption("Roger", self.nextLevel)
        self.Menu.addMultiLineAdditionalText("Commander, beta squad have been converted into those things" )
        self.Menu.addMultiLineAdditionalText("we hate to order this, but they must be destroyed. Be careful, we dont know " )
        self.Menu.addMultiLineAdditionalText("how they became infected.  " )
        self.Menu.addMultiLineAdditionalText("our investigation is nearing the source of the infection.")

        self.Menu.setTitle("Level 2")
        self.nextScreen = None
        

########################################################################
    def nextLevel(self):
        self.nextScreen = Level2()

        
#######################################################
###############Generic Calls###########################
#######################################################         
    def draw(self):
        self.Menu.draw()

#######################################################          
    def update(self):
        self.Menu.update()

#######################################################          
    def handleEvent(self, event):
        self.Menu.handleEvent(event)