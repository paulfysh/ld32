import math

##################################################################
def remove90FromAngle(angle):
    tempAngle = angle
    tempAngle -= 90
    
    if(tempAngle > 180):
        tempAngle -= 360
    if(tempAngle < -180):
        tempAngle += 360
    
    return tempAngle

#################################################################
def handleAngleBoundary(angle):
    if(angle < -180):
        return angle + 360
    if(angle > 180):
        return angle - 360
        
    return angle


##################################################################
def getXYForAngleAndRange(angle, distance):
    testAngle = remove90FromAngle(angle)
        
    destinationY = distance * math.sin(math.radians(testAngle))
    destinationX = distance * math.cos(math.radians(testAngle))
        
    return (destinationX, destinationY)

##################################################################
def getAngleToFaceCoord(coordXY, centerX, centerY):        
    if( (not coordXY[0] == centerX) or (not coordXY[1] == centerY) ):
        dx = coordXY[0] - centerX
        dy = coordXY[1] - centerY
        rads = math.atan2(dx, -dy)
        angle = math.degrees(rads) 
        return angle
    
    
#################################################################    
def distanceBetweenCoordinates(firstCoordXY, secondCoordXY):
    xVal = secondCoordXY[0] - firstCoordXY[0]
    yVal = secondCoordXY[1] - firstCoordXY[1]
    xSquare = math.pow(xVal, 2)
    ySquare = math.pow(yVal, 2)
    
    distance = math.sqrt(xSquare + ySquare)
    
    return distance

#################################################################  
def getPositionPostRotation(originalPosition, center, angle):
#http://stackoverflow.com/questions/6428192/get-new-x-y-coordinates-of-a-point-in-a-rotated-image
#         function rotate(x, y, xm, ym, a) {
#         var cos = Math.cos,
#         sin = Math.sin,
# 
#         a = a * Math.PI / 180, // Convert to radians because that's what
#                                // JavaScript likes
# 
#         // Subtract midpoints, so that midpoint is translated to origin
#         // and add it in the end again
#         xr = (x - xm) * cos(a) - (y - ym) * sin(a)   + xm,
#         yr = (x - xm) * sin(a) + (y - ym) * cos(a)   + ym;
# 
#         return [xr, yr];
#         }
#            rotate(16, 32, 16, 16, 30); // [8, 29.856...]
    
    angleInRadians = math.radians(angle)
    
    rotatedX = (originalPosition[0] - center[0]) * math.cos(angleInRadians) - (originalPosition[1] - center[1]) * math.sin(angleInRadians) + center[0]
    rotatedY = (originalPosition[0] - center[0]) * math.sin(angleInRadians) + (originalPosition[1] - center[1]) * math.cos(angleInRadians) + center[1]
    
    return (rotatedX, rotatedY)