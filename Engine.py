import sys
from Menu import Menu
from Prologue import Prologue
from InstructionsMenu import InstructionMenu

class Engine(object):
    
#######################################################
    def __init__(self):
        self.currentScreen = None
        self.lastScreen = None
        self.setupMainMenu()
        
#######################################################
###############Generic Calls###########################
#######################################################    
    def update(self):
        if(self.currentScreen):
            self.currentScreen.update()
            
            if(self.currentScreen.nextScreen):
                if(self.currentScreen.nextScreen == "Back"):
                    self.currentScreen = self.lastScreen
                else:
                    self.currentScreen = self.currentScreen.nextScreen
        

#######################################################        
    def draw(self):
        if(self.currentScreen):
            self.currentScreen.draw()

#######################################################        
    def handleEvent(self, event):
        if(self.currentScreen):
            self.currentScreen.handleEvent(event)
        
        
#######################################################
    def setupMainMenu(self):
        self.currentScreen = Menu()
        self.currentScreen.addMenuOption("New Game", self.newGame)
        self.currentScreen.addMenuOption("Instructions", self.instructions)
        self.currentScreen.addMenuOption("Quit", self.quitGame)
        self.currentScreen.setTitle("Bioterrorism")
        
#######################################################
############Menu callbacks#############################
#######################################################
    def newGame(self):
        self.currentScreen = Prologue()

#######################################################    
    def quitGame(self):
        sys.exit()

#######################################################         
    def instructions(self):
        self.lastScreen = self.currentScreen
        self.currentScreen = InstructionMenu() 
        


        
        

