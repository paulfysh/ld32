import pygame
import Globals
import MathsHelpers
import math
from GameObject import GameObject

class AIUnit(GameObject):
    CIV=0
    SOLDIERZOMBIE=1
    SOLDIER=2

##########################################      
    def __init__(self, unitType, topLeft):
        super(AIUnit, self).__init__(topLeft)
        
        self.lungeAttack = None
        
        if(unitType == AIUnit.CIV):
            self.stationaryImage = pygame.image.load("Assets/ZombieStationaryScaled.png")
            self.leftFootImage = pygame.image.load("Assets/ZombieLeftFootForwardScaled.png")
            self.rightFootImage = pygame.image.load("Assets/ZombieRightFootForwardScaled.png")
            
            self.lunge1Image = pygame.image.load("Assets/ZombieGrabFrame1Scaled.png")
            self.lunge2Image = pygame.image.load("Assets/ZombieGrabFrame2Scaled.png")
            
            self.lungeAttack = True
            
            self.attackValue = 4
            self.defenseValue = 5
            self.armour = 0
            self.maxRange = 0 #in tiles 0 means local only
        elif(unitType == AIUnit.SOLDIERZOMBIE):
            self.stationaryImage = pygame.image.load("Assets/ZombieSoldierStationaryScaled.png")
            self.leftFootImage = pygame.image.load("Assets/ZombieSoldierLeftFootForwardScaled.png")
            self.rightFootImage = pygame.image.load("Assets/ZombieSoldierRightFootForwardScaled.png")
            
            #projectile
            self.projectileImage = pygame.image.load("Assets/projectile.png")
            self.projectileRect = self.projectileImage.get_rect()
            
            self.projectileCreationCoord = (27,0)
            self.lungeAttack = False
            
            #make them worse than you, zombies cant really aim afterall. 
            self.attackValue = 5
            self.defenseValue = 5
            self.armour = 0
            self.maxRange = 3
        elif(unitType == AIUnit.SOLDIER):
            #the best, and end boss, make them better than you but shorter range
            self.stationaryImage = pygame.image.load("Assets/EnemySoldierStationaryScaled.png")
            self.leftFootImage = pygame.image.load("Assets/EnemySoldierLeftFootForwardScaled.png")
            self.rightFootImage = pygame.image.load("Assets/EnemySoldierRightFootForwardScaled.png")
            
            #projectile
            self.projectileImage = pygame.image.load("Assets/projectile.png")
            self.projectileRect = self.projectileImage.get_rect()
            
            self.projectileCreationCoord = (27,0)
            self.lungeAttack = False
            
            #make them worse than you, zombies cant really aim afterall. 
            self.attackValue = 3
            self.defenseValue = 3
            self.armour = 0
            self.maxRange = 2
        
        self.currentRotation = 180
        
        self.moan = pygame.mixer.Sound("Assets/Sounds/ZombieMoan.wav")
        self.gunShot = pygame.mixer.Sound("Assets/Sounds/Gun.wav")
        
        #movement
        self.walkingList = []
        self.walkingList.append(self.stationaryImage)
        self.walkingList.append(self.leftFootImage)
        self.walkingList.append(self.stationaryImage)
        self.walkingList.append(self.leftFootImage) #lets see if this looks like a bit of a lurch
        self.walkingList.append(self.rightFootImage)
        
        self.currentImage = pygame.transform.rotate(self.stationaryImage, self.currentRotation)
        self.workingImage = self.stationaryImage
        
        self.positionRect = self.currentImage.get_rect()
        self.positionRect.topleft = topLeft
        self.realCurrentPosition = self.positionRect.center
        
        self.movePerSecond = float(75.0)
        self.currentAngle = self.currentRotation
        self.intervalBetweenSteps = 0.1
        self.lastStepChange = None
                
        #combat animation
        self.intervalBetweenCombatFrames = 0.3
        self.lastCombatFrameTime = None
        self.fighting = False
        self.nextImageToUse = 0
        
        self.currentDestination = self.positionRect.center
        
        #shooting combat
        self.activeProjectile = False
        self.projectileMovePerSecond = float(300.0)
        self.targetRect = None
        
            
##########################################    
    def draw(self):
        super(AIUnit, self).draw()
        Globals.screen.blit(self.currentImage, self.positionRect)
        
        if(self.activeProjectile):
            Globals.screen.blit(self.projectileImage, self.projectileRect)
 
##########################################        
    def setDestinationViaTarget(self, target, distanceToMove):
        angleToTarget = MathsHelpers.handleAngleBoundary(MathsHelpers.getAngleToFaceCoord(target.getCenterPosition(), self.positionRect.center[0], self.positionRect.center[1]))
        amountToAddToPos = MathsHelpers.getXYForAngleAndRange(angleToTarget, distanceToMove)
        self.setDestination( (math.floor(self.getCenterPosition()[0] + amountToAddToPos[0]), (math.floor(self.getCenterPosition()[1] + amountToAddToPos[1])) ) )
 
##########################################        
    def handleCombat(self, targetRect):
        self.updateAngle(targetRect.center)
        if(self.lungeAttack):
            self.currentImage = pygame.transform.rotate(self.lunge1Image, self.currentAngle)
            self.lastCombatFrameTime = Globals.currentTime
            self.fighting = True
            self.nextImageToUse = 1
            self.moan.play()
        else:
            self.targetRect = targetRect
            self.projectileRect = self.projectileImage.get_rect()
            
            weaponBarellPos = (self.positionRect.topleft[0] + self.projectileCreationCoord[0], self.positionRect.topleft[1] + self.projectileCreationCoord[1])
            self.projectileRect.center = MathsHelpers.getPositionPostRotation(weaponBarellPos, self.positionRect.center, self.currentAngle)
            self.activeProjectile = True
            self.gunShot.play()


##########################################     
    def combatHasFinished(self): #default this, zombie civs dont shoot, so we can assume they have stopped and players can override. 
        if(self.lungeAttack):
            return (not self.fighting)
        else:
            return not self.activeProjectile

##########################################           
    def update(self):
        super(AIUnit, self).update()
        
        if(self.lungeAttack):
            if(self.fighting):
                if(Globals.currentTime >= self.lastCombatFrameTime + self.intervalBetweenCombatFrames):
                    if(self.nextImageToUse == 1):
                        self.currentImage = pygame.transform.rotate(self.lunge2Image, self.currentAngle)
                        self.nextImageToUse = 2
                    else:
                        self.currentImage = pygame.transform.rotate(self.stationaryImage, self.currentAngle)
                        self.fighting = False
        else:
            if(self.activeProjectile):
                projectileMoveThisTick = self.projectileMovePerSecond / Globals.FPS        
                projectileAngle = MathsHelpers.getAngleToFaceCoord(self.targetRect.center, self.projectileRect.center[0], self.projectileRect.center[1])
                incrementPositionBy = MathsHelpers.getXYForAngleAndRange(projectileAngle, projectileMoveThisTick)
                self.projectileRect.center = (self.projectileRect.center[0] + incrementPositionBy[0], self.projectileRect.center[1] + incrementPositionBy[1])
                
                if(self.projectileRect.colliderect(self.targetRect)):
                    self.activeProjectile = False
            
            
            
            
        
        
        
