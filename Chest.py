import pygame
import Globals

from GameObject import GameObject

########################################## 
class Chest(GameObject):
    HEAL = 0
    GAS = 1
    WPNUPGRADE = 2
    ARMOURUPGRADE = 3
    
    def __init__(self, chestType, topLeft):
        super(Chest, self).__init__(topLeft)
        self.closedImage = pygame.image.load("Assets/ClosedChestScaled.png")
        self.openImage = pygame.image.load("Assets/OpenChestScaled.png")
        self.positionRect = self.closedImage.get_rect()
        self.positionRect.topleft = topLeft
        
        self.healImage = pygame.image.load("Assets/heal.png")
        self.healRect = self.healImage.get_rect()
        self.healRect.center = self.positionRect.center
        
        self.armourImage = pygame.image.load("Assets/armourUpgrade.png")
        self.armourRect = self.armourImage.get_rect()
        self.armourRect.center = self.positionRect.center
        
        self.weaponImage = pygame.image.load("Assets/wpnUpgrade.png")
        self.weaponRect = self.weaponImage.get_rect()
        self.weaponRect.center = self.positionRect.center
        
        self.gasImage = pygame.image.load("Assets/GasCloudCircleScaled.png")
        self.gasRect = self.gasImage.get_rect()
        self.gasRect.center = self.positionRect.center
        
        self.upgradeImageDisplayDuration = 1
        self.upgradeImageStarted = None
        self.upgradeImageToDisplay = None
        self.upgradeRectToUse = None
        
        self.isOpen = False
        self.contents = chestType

##########################################  
    def open(self, openingUnit):
        #do good or bad stuff here
        self.upgradeImageStarted = Globals.currentTime
        
        if(self.contents == Chest.HEAL):
            self.upgradeImageToDisplay = self.healImage
            self.upgradeRectToUse = self.healRect
            openingUnit.doHeal()
        elif(self.contents == Chest.GAS):
            self.upgradeImageToDisplay = self.gasImage
            self.upgradeRectToUse = self.gasRect
            openingUnit.doGasAttack()
        elif(self.contents == Chest.WPNUPGRADE):
            self.upgradeImageToDisplay = self.weaponImage
            self.upgradeRectToUse = self.weaponRect
            openingUnit.doWeaponUpgrade()
        elif(self.contents == Chest.ARMOURUPGRADE):
            self.upgradeImageToDisplay = self.armourImage
            self.upgradeRectToUse = self.armourRect
            openingUnit.doDefenseUpgrade()
    
        self.isOpen = True
        
        
##########################################         
    def draw(self):
        if(self.isOpen):
            Globals.screen.blit(self.openImage, self.positionRect)
            if(self.upgradeImageStarted + self.upgradeImageDisplayDuration >= Globals.currentTime):
                Globals.screen.blit(self.upgradeImageToDisplay, self.upgradeRectToUse)
        else:
            Globals.screen.blit(self.closedImage, self.positionRect)
            
