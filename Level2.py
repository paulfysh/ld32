import pygame
import Globals
from CoverItem import CoverItem
from PlayerUnit import PlayerUnit
from Map import Map
from AIUnit import AIUnit
from Chest import Chest
from DefeatMenu import DefeatMenu
from Level3IntroMenu import Level3IntroMenu

from random import randint

class Level2(object):
#######################################################
    def __init__(self):
        self.map = Map(6, 6)
        
        self.nextScreen = None
        
        self.setupMap()


#######################################################
    def setupMap(self):
        #add some items to the map
        barell = CoverItem(CoverItem.UPRIGHTBARREL, (25, 425))
        sidewaysBarell = CoverItem(CoverItem.SIDEWAYSBARREL, (225, 425))
        sandbags = CoverItem(CoverItem.SANDBAGS, (525, 425))
        barell2 = CoverItem(CoverItem.UPRIGHTBARREL, (25, 75))
        barell3 = CoverItem(CoverItem.UPRIGHTBARREL, (125, 75))
        barell4 = CoverItem(CoverItem.UPRIGHTBARREL, (225, 75))
        
        self.map.addCoverItemToMap(barell)
        self.map.addCoverItemToMap(sidewaysBarell)
        self.map.addCoverItemToMap(sandbags)
        self.map.addCoverItemToMap(barell2)
        self.map.addCoverItemToMap(barell3)
        self.map.addCoverItemToMap(barell4)
        
        soldier = PlayerUnit(PlayerUnit.REGULAR, (25, 526 + barell.getSize()[1]))
        soldier2 = PlayerUnit(PlayerUnit.REGULAR, (225, 526 + sidewaysBarell.getSize()[1]))
        soldier3 = PlayerUnit(PlayerUnit.REGULAR, (525, 526 + sandbags.getSize()[1]))
        
        self.map.addFriendlyUnitToMap(soldier)
        self.map.addFriendlyUnitToMap(soldier2)
        self.map.addFriendlyUnitToMap(soldier3)
        
        zombie = AIUnit(AIUnit.SOLDIERZOMBIE, (25, 25))
        zombie1 = AIUnit(AIUnit.SOLDIERZOMBIE, (125, 25))
        zombie2 = AIUnit(AIUnit.SOLDIERZOMBIE, (225, 25))
        zombie3 = AIUnit(AIUnit.CIV, (325, 25))
        zombie4 = AIUnit(AIUnit.CIV, (425, 25)) 
        
        self.map.addAIUnitToMap(zombie)
        self.map.addAIUnitToMap(zombie1)
        self.map.addAIUnitToMap(zombie2)
        self.map.addAIUnitToMap(zombie3)
        self.map.addAIUnitToMap(zombie4)
        
        chest = Chest(Chest.WPNUPGRADE, (150, 350))
        self.map.addChestToMap(chest)
        
        chest2 = Chest(Chest.ARMOURUPGRADE, (350, 350))
        self.map.addChestToMap(chest2)
        
        chest3 = Chest(randint(0,3), (250, 250))
        self.map.addChestToMap(chest3)


#######################################################
###############Generic Calls###########################
#######################################################           
    def draw(self):
        self.map.draw()

#######################################################        
    def update(self):
        self.map.update()
        
        if(self.map.getNumFriendlys() == 0):
            self.nextScreen = DefeatMenu()
        elif(self.map.getNumAis() == 0):
            self.nextScreen = Level3IntroMenu()

#######################################################        
    def handleEvent(self, event):
        self.map.handleEvent(event)