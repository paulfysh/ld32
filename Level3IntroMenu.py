import sys
from Menu import Menu
from Level3 import Level3

class Level3IntroMenu(object):
########################################################################
    def __init__(self):
        self.Menu = Menu()
        self.Menu.addMenuOption("With Pleasure!", self.nextLevel)
        self.Menu.addMultiLineAdditionalText("Commander, well done, that kind of thing is never easy." )
        self.Menu.addMultiLineAdditionalText("We have finally tracked down the culprets, a group of bio terrorists with no known affiliation" )
        self.Menu.addMultiLineAdditionalText("we are sending you to destroy them, but they know you are coming. Expect some traps, and Good hunting ")

        self.Menu.setTitle("Level 3")
        self.nextScreen = None
        

########################################################################
    def nextLevel(self):
        self.nextScreen = Level3()

        
#######################################################
###############Generic Calls###########################
#######################################################         
    def draw(self):
        self.Menu.draw()

#######################################################          
    def update(self):
        self.Menu.update()

#######################################################          
    def handleEvent(self, event):
        self.Menu.handleEvent(event)