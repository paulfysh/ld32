import pygame
import Globals
import math

import MathsHelpers

from GameObject import GameObject

class PlayerUnit(GameObject):
    REGULAR = 0
##########################################  
    def __init__(self, unitType, topLeft):
        super(PlayerUnit, self).__init__(topLeft)
        if(unitType == PlayerUnit.REGULAR):
            self.stationaryImage = pygame.image.load("Assets/SoldierStationaryScaled.png")
            self.leftFootImage = pygame.image.load("Assets/SoldierLeftFootForwardScaled.png")
            self.rightFootImage = pygame.image.load("Assets/SoldierRightFootForwardScaled.png")
            
            self.positionRect = self.stationaryImage.get_rect()
            self.positionRect.topleft = topLeft
            self.realCurrentPosition = self.positionRect.center
            
            self.attackValue = 4
            self.defenseValue = 4
            self.armour = 0 #was one but it took ages before things damaged me.
            self.maxRange = 3 #in tiles
            
            self.projectileCreationCoord = (27,0)
        
        #projectile
        self.projectileImage = pygame.image.load("Assets/projectile.png")
        self.projectileRect = self.projectileImage.get_rect()
        
        #movement
        self.walkingList = []
        self.walkingList.append(self.stationaryImage)
        self.walkingList.append(self.leftFootImage)
        self.walkingList.append(self.rightFootImage)
        
        self.currentImage = self.stationaryImage
        self.workingImage = self.stationaryImage
        
        self.movePerSecond = float(100.0)
        self.currentAngle = 0
        self.intervalBetweenSteps = 0.1
        self.lastStepChange = None
        
        #combat
        self.activeProjectile = False
        self.projectileMovePerSecond = float(300.0)
        self.targetRect = None
        
        self.currentDestination = self.positionRect.center
        
        self.hasBeenGassed = False
        
        self.gunShot = pygame.mixer.Sound("Assets/Sounds/Gun.wav")      

 
##########################################        
    def handleCombat(self, targetRect):
        self.updateAngle(targetRect.center)
        self.targetRect = targetRect
        self.projectileRect = self.projectileImage.get_rect()
        
        weaponBarellPos = (self.positionRect.topleft[0] + self.projectileCreationCoord[0], self.positionRect.topleft[1] + self.projectileCreationCoord[1])
        self.projectileRect.center = MathsHelpers.getPositionPostRotation(weaponBarellPos, self.positionRect.center, self.currentAngle)
        self.activeProjectile = True
        self.gunShot.play()

##########################################         
    def combatHasFinished(self):
        return not self.activeProjectile
        
        

##########################################    
    def draw(self):
        super(PlayerUnit, self).draw()
        Globals.screen.blit(self.currentImage, self.positionRect)
            
        if(self.activeProjectile):
            Globals.screen.blit(self.projectileImage, self.projectileRect)

##########################################            
    def update(self):
        super(PlayerUnit, self).update()
        #handle projectile
        if(self.activeProjectile):
            projectileMoveThisTick = self.projectileMovePerSecond / Globals.FPS        
            projectileAngle = MathsHelpers.getAngleToFaceCoord(self.targetRect.center, self.projectileRect.center[0], self.projectileRect.center[1])
            incrementPositionBy = MathsHelpers.getXYForAngleAndRange(projectileAngle, projectileMoveThisTick)
            self.projectileRect.center = (self.projectileRect.center[0] + incrementPositionBy[0], self.projectileRect.center[1] + incrementPositionBy[1])
            
            if(self.projectileRect.colliderect(self.targetRect)):
                self.activeProjectile = False

##########################################                 
    def doHeal(self):
        self.currentHealth = self.maxHealth
 
 ##########################################       
    def doWeaponUpgrade(self):
        if(self.attackValue > 1):
            self.attackValue -= 1

 ##########################################            
    def doDefenseUpgrade(self):
        if(self.defenseValue > 1):
            self.defenseValue -= 1

 ##########################################               
    def doGasAttack(self):
        self.hasBeenGassed = True
            
            
        
            