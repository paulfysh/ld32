import pygame
import Globals

class MapTile(object):
##########################################
    def __init__(self, topLeftPosition):
        self.image = pygame.image.load("Assets/SandTile.png")
        self.bloodstainImage = pygame.image.load("Assets/BloodStain.png")
        self.imageRect = self.image.get_rect()
        self.imageRect.topleft = topLeftPosition
        
        self.coverObjects = []
        self.playerUnits = []
        self.aiUnits = []
        self.chests = []
        self.bloodStains = []
        
##########################################        
    def addCover(self, coverObject):
        self.coverObjects.append(coverObject)

##########################################    
    def addFriendlyUnit(self, friendlyUnit):
        self.playerUnits.append(friendlyUnit)

##########################################         
    def removeFriendlyUnit(self, friendlyUnit):
        self.playerUnits.remove(friendlyUnit)

##########################################           
    def getAllFriendlyUnits(self):
        return self.playerUnits
    
##########################################        
    def addAiUnit(self, aiUnit):
        self.aiUnits.append(aiUnit)

##########################################        
    def removeAiUnit(self, aiUnit):
        self.aiUnits.remove(aiUnit)
        
##########################################   
    def getAllAiUnits(self):
        return self.aiUnits
    
##########################################    
    def addChest(self, chest):
        self.chests.append(chest)

##########################################         
    def addBloodStain(self, pos):
        newRect = self.bloodstainImage.get_rect()
        newRect.center = pos
        self.bloodStains.append(newRect)

##########################################         
    def coordinateIsInside(self, coord):
        if( (coord[0] >= self.imageRect.left and coord[0] <= self.imageRect.right) and
            (coord[1] >= self.imageRect.top and coord[1] <= self.imageRect.bottom) ):
            return True
        return False

##########################################    
    def getSize(self):
        return self.imageRect.size
    
##########################################  
    def tileProvidesCover(self):
        if(len(self.coverObjects > 0) or len(self.chests > 0)):
            return True
    
        return False

##########################################      
    def getItemUnderMouse(self, mousePos):
        for unit in self.playerUnits:
            if(unit.coordinateIsInside(mousePos)):
                return unit
            
        for unit in self.aiUnits:
            if(unit.coordinateIsInside(mousePos)):
                return unit
            
        for chest in self.chests:
            if(chest.coordinateIsInside(mousePos)):
                return chest
        
        return None

##########################################    
    def getNumFriendlys(self):
        return len(self.playerUnits)

##########################################        
    def getNumAis(self):
        return len(self.aiUnits)
        
##########################################         
    def draw(self, highlight):
        Globals.screen.blit(self.image, self.imageRect)
        
        if(highlight):
            #draw a green border round the tile to show we can move there. 
            pygame.draw.rect(Globals.screen, pygame.Color("Blue"), (self.imageRect.left - 1, self.imageRect.top - 1, self.imageRect.width + 2, self.imageRect.height + 2), 1)
        

##########################################             
    def drawObjectsOnTile(self):
        for stain in self.bloodStains:
            Globals.screen.blit(self.bloodstainImage, stain)
        
        for cover in self.coverObjects:
            cover.draw()
        
        for unit in self.playerUnits:
            unit.draw()
        
        for unit in self.aiUnits:
            unit.draw()
            
        for chest in self.chests:
            chest.draw()
            
        
        
##########################################  
    def update(self):
        for unit in self.playerUnits:
            unit.update()    
        
        for unit in self.aiUnits:
            unit.update()    
    
        