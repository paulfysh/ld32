import pygame
import Globals
from CoverItem import CoverItem
from PlayerUnit import PlayerUnit
from Map import Map
from AIUnit import AIUnit
from Chest import Chest
from DefeatMenu import DefeatMenu
from Level2IntroMenu import Level2IntroMenu

from random import randint

class Level1(object):
#######################################################
    def __init__(self):
        self.map = Map(6, 6)
        
        self.nextScreen = None
        
        self.setupMap()


#######################################################
    def setupMap(self):
        #add some items to the map
        barell = CoverItem(CoverItem.UPRIGHTBARREL, (25, 525))
        sidewaysBarell = CoverItem(CoverItem.SIDEWAYSBARREL, (225, 525))
        sandbags = CoverItem(CoverItem.SANDBAGS, (525, 525))
        
        self.map.addCoverItemToMap(barell)
        self.map.addCoverItemToMap(sidewaysBarell)
        self.map.addCoverItemToMap(sandbags)
        
        soldier = PlayerUnit(PlayerUnit.REGULAR, (25, 526 + barell.getSize()[1]))
        soldier2 = PlayerUnit(PlayerUnit.REGULAR, (225, 526 + sidewaysBarell.getSize()[1]))
        soldier3 = PlayerUnit(PlayerUnit.REGULAR, (525, 526 + sandbags.getSize()[1]))
        
        self.map.addFriendlyUnitToMap(soldier)
        self.map.addFriendlyUnitToMap(soldier2)
        self.map.addFriendlyUnitToMap(soldier3)
        
        zombie = AIUnit(AIUnit.CIV, (25, 25))
        zombie1 = AIUnit(AIUnit.CIV, (125, 25))
        zombie2 = AIUnit(AIUnit.CIV, (225, 25))
        zombie3 = AIUnit(AIUnit.CIV, (325, 25))
        zombie4 = AIUnit(AIUnit.CIV, (425, 25))
        zombie5 = AIUnit(AIUnit.CIV, (525, 25)) 
        #zombie4 = PlayerUnit(PlayerUnit.REGULAR, (425, 25))#AIUnit(AIUnit.CIV, (425, 25)) 
        
        self.map.addAIUnitToMap(zombie)
        self.map.addAIUnitToMap(zombie1)
        self.map.addAIUnitToMap(zombie2)
        self.map.addAIUnitToMap(zombie3)
        self.map.addAIUnitToMap(zombie4)
        self.map.addAIUnitToMap(zombie5)
        
        chest = Chest(randint(0,3), (350, 350))
        self.map.addChestToMap(chest)


#######################################################
###############Generic Calls###########################
#######################################################           
    def draw(self):
        self.map.draw()

#######################################################        
    def update(self):
        self.map.update()
        
        if(self.map.getNumFriendlys() == 0):
            self.nextScreen = DefeatMenu()
        elif(self.map.getNumAis() == 0):
            self.nextScreen = Level2IntroMenu()

#######################################################        
    def handleEvent(self, event):
        self.map.handleEvent(event)